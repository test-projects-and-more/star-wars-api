import { type SVGProps } from 'react'
const NoneGender = (props: SVGProps<SVGSVGElement>) => (
  <svg
  xmlns="http://www.w3.org/2000/svg"
  xmlSpace="preserve"
  style={{
    background: 'new 0 0 48 48'
  }}
  viewBox="0 0 48 48"
  {...props}
>
  <path
    d="M31.854 20.108a12.315 12.315 0 0 1 4.484 9.512c0 6.813-5.526 12.339-12.338 12.339-6.812 0-12.338-5.526-12.338-12.339 0-4.692 2.627-8.778 6.486-10.862"
    style={{
      fill: '#f4f4f4'
    }}
  />
  <path
    d="m19.749 22.336-5.229 7.58 1.381 3.58 11.204.375 2.497-3.512L24 22.336z"
    style={{
      fill: '#e0e0e0'
    }}
  />
  <path
    d="M14.52 37.515c9.914-7.804 15.28 2.997 15.28 2.997a5.823 5.823 0 0 0 2.302-1.59s-1.926-5.633-8.38-6.626c-6.452-.992-10.546 2.824-10.546 2.824.309 1.027.756 1.827 1.344 2.395zM11.662 29.62c6.673-2.898 5.366-10.177 5.366-10.177l1.12-.685 1.624.713s1.402 8.891-7.818 12.825M26.117 20.41s-.347 9.125 10.06 9.506c.093 1.111.053 2.144-.284 2.997C22.32 31.92 23.327 19.87 23.327 19.87"
    style={{
      fill: '#ffa001'
    }}
  />
  <path
    d="M32.578 19.091s1.218-7.676-6.276-8.972c0 0-7.104-1.4-8.868 7.416"
    style={{
      fill: '#f4f4f4'
    }}
  />
  <path
    d="m32.578 19.091-15.144-1.556 1.038 1.79 12.913 1.451z"
    style={{
      fill: '#c9c9c9'
    }}
  />
  <circle
    cx={25.761}
    cy={14.256}
    r={2.738}
    style={{
      fill: '#464747'
    }}
  />
  <circle
    cx={25.761}
    cy={14.256}
    r={1.344}
    style={{
      fill: '#707070'
    }}
  />
  <path
    d="M29.97 18.822h-.009M31.854 20.108a12.315 12.315 0 0 1 4.484 9.512c0 6.813-5.526 12.339-12.338 12.339-6.812 0-12.338-5.526-12.338-12.339 0-4.692 2.627-8.778 6.486-10.862"
    style={{
      fill: 'none',
      stroke: '#303030',
      strokeWidth: 0.75,
      strokeLinecap: 'round',
      strokeLinejoin: 'round',
      strokeMiterlimit: 10
    }}
  />
  <path
    d="m32.578 19.091-15.144-1.556 1.038 1.79 12.913 1.451z"
    style={{
      fill: 'none',
      stroke: '#303030',
      strokeWidth: 0.75,
      strokeLinecap: 'round',
      strokeLinejoin: 'round',
      strokeMiterlimit: 10
    }}
  />
  <path
    d="M32.578 19.091s1.218-7.676-6.276-8.972c0 0-7.104-1.4-8.868 7.416"
    style={{
      fill: 'none',
      stroke: '#303030',
      strokeWidth: 0.75,
      strokeLinecap: 'round',
      strokeLinejoin: 'round',
      strokeMiterlimit: 10
    }}
  />
  <circle
    cx={25.761}
    cy={14.256}
    r={2.738}
    style={{
      fill: 'none',
      stroke: '#303030',
      strokeWidth: 0.75,
      strokeLinecap: 'round',
      strokeLinejoin: 'round',
      strokeMiterlimit: 10
    }}
  />
  <circle
    cx={25.761}
    cy={14.256}
    r={1.344}
    style={{
      fill: 'none',
      stroke: '#303030',
      strokeWidth: 0.75,
      strokeLinecap: 'round',
      strokeLinejoin: 'round',
      strokeMiterlimit: 10
    }}
  />
  <circle
    cx={20.937}
    cy={15.386}
    r={0.804}
    style={{
      fill: '#303030'
    }}
  />
  <path
    d="m24.611 10.063.094-1.413M23.239 10.295l.176-3.514"
    style={{
      fill: 'none',
      stroke: '#303030',
      strokeWidth: 0.75,
      strokeLinecap: 'round',
      strokeLinejoin: 'round',
      strokeMiterlimit: 10
    }}
  />
  <circle
    cx={24.705}
    cy={8.215}
    r={0.707}
    style={{
      fill: '#303030'
    }}
  />
  <circle
    cx={23.435}
    cy={6.373}
    r={0.707}
    style={{
      fill: '#303030'
    }}
  />
  <path
    d="M26.117 20.41s-.347 9.125 10.06 9.506"
    style={{
      fill: 'none',
      stroke: '#303030',
      strokeWidth: 0.75,
      strokeMiterlimit: 10
    }}
  />
  <path
    d="M23.327 19.87S22.32 31.92 35.893 32.913M17.028 19.443s1.307 7.28-5.366 10.177"
    style={{
      fill: 'none',
      stroke: '#303030',
      strokeWidth: 0.75,
      strokeMiterlimit: 10
    }}
  />
  <path
    d="M19.772 19.47s1.402 8.892-7.818 12.826M29.8 40.512s-5.366-10.801-15.28-2.997"
    style={{
      fill: 'none',
      stroke: '#303030',
      strokeWidth: 0.75,
      strokeMiterlimit: 10
    }}
  />
  <path
    d="M32.102 38.923s-1.926-5.634-8.38-6.627c-6.452-.992-10.546 2.824-10.546 2.824M29.143 31.047l-1.889 2.416M14.911 30.922l.99 2.221M23.31 22.284l-3.462.104"
    style={{
      fill: 'none',
      stroke: '#303030',
      strokeWidth: 0.75,
      strokeMiterlimit: 10
    }}
  />
  <circle
    cx={31.147}
    cy={33.871}
    r={0.59}
    style={{
      fill: '#303030'
    }}
  />
  <circle
    cx={20.937}
    cy={28.535}
    r={0.59}
    style={{
      fill: '#303030'
    }}
  />
  <path
    d="M30.833 20.776s2.423 3.137 2.804 4.715M33.909 26.744l.136.843M17.67 38.013s4.244 3.316 9.162 1.715"
    style={{
      fill: 'none',
      stroke: '#303030',
      strokeWidth: 0.75,
      strokeLinecap: 'round',
      strokeMiterlimit: 10
    }}
  />
</svg>
)
export default NoneGender
