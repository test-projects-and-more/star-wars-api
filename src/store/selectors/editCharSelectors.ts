import { type RootState } from '../reducers'

export const getIsEditSelector = (state: RootState) => state.editChar.isEdit
