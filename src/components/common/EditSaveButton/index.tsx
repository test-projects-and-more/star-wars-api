import { Button } from '@mui/material'
import { type FC } from 'react'

export interface IEditSaveButtonProps {
  isEdit: boolean
  onClick: React.MouseEventHandler<HTMLButtonElement>
}

const EditSaveButton: FC<IEditSaveButtonProps> = ({ isEdit, onClick }) => {
  return isEdit
    ? (
    <Button onClick={onClick} color="success" variant="contained">
      Save
    </Button>
      )
    : (
    <Button onClick={onClick} variant="contained">
      Edit
    </Button>
      )
}

export default EditSaveButton
