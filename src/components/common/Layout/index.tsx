import { Container } from '@mui/material'
import { Outlet } from 'react-router-dom'
import Header from '../Header'

const Layout = () => (
  <>
    <Header />
    <Container maxWidth="lg">
      <Outlet />
    </Container>
  </>
)

export default Layout
