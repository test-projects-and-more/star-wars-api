import { fireEvent, render, screen } from '@testing-library/react'

import EditSaveButton, { type IEditSaveButtonProps } from '.'

describe('EditSaveButton', () => {
  const onClickMock = jest.fn()
  const defaultProps: IEditSaveButtonProps = {
    isEdit: false,
    onClick: onClickMock
  }

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('renders "Edit" button when isEdit is false', () => {
    render(<EditSaveButton {...defaultProps} />)

    const editButton = screen.getByRole('button', { name: /edit/i })
    expect(editButton).toBeInTheDocument()
  })

  it('renders "Save" button when isEdit is true', () => {
    const props: IEditSaveButtonProps = {
      ...defaultProps,
      isEdit: true
    }

    render(<EditSaveButton {...props} />)

    const saveButton = screen.getByRole('button', { name: /save/i })
    expect(saveButton).toBeInTheDocument()
  })

  it('calls onClick handler when button is clicked', () => {
    render(<EditSaveButton {...defaultProps} />)

    const button = screen.getByRole('button')
    fireEvent.click(button)

    expect(onClickMock).toHaveBeenCalledTimes(1)
  })
})
