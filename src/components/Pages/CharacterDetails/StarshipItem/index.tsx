import { TableCell, TableRow } from '@mui/material'
import { type FC } from 'react'
import { useGetStarshipQuery } from '../../../../api/swapi'
import Loader from '../../../common/Loader'

interface IStarshipItemProp {
  starshipId: string
}

const StarshipItem: FC<IStarshipItemProp> = ({ starshipId }) => {
  const { data: starship, isLoading } = useGetStarshipQuery(starshipId)

  if (isLoading) {
    return (
      <TableRow>
        <TableCell>
          <Loader />
        </TableCell>
      </TableRow>
    )
  }

  if (!starship) {
    return <TableRow>Error</TableRow>
  }

  const { name, model, starship_class, cost_in_credits } = starship

  return (
    <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
      <TableCell>{name}</TableCell>
      <TableCell>{model}</TableCell>
      <TableCell>{starship_class}</TableCell>
      <TableCell>{cost_in_credits}</TableCell>
    </TableRow>
  )
}

export default StarshipItem
