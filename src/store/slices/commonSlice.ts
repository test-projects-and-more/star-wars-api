import { createSlice, type PayloadAction } from '@reduxjs/toolkit'

interface ICommonState {
  darkMode: boolean
  search: string
}

const initialState: ICommonState = {
  darkMode: false,
  search: ''
}

const commonSlice = createSlice({
  name: 'common',
  initialState,
  reducers: {
    toggleDarkMode: (state) => {
      state.darkMode = !state.darkMode
    },
    setSearch: (state, action: PayloadAction<string>) => {
      state.search = action.payload
    }
  }
})

export const { toggleDarkMode, setSearch } = commonSlice.actions
export default commonSlice.reducer
