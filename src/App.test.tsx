import App from './App'
import { renderWithRedux } from './tests/helpers/renderWithRedux'

test('App renders without errors', () => {
  renderWithRedux(
    <App />
  )
})
