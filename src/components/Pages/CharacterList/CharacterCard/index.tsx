import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Typography
} from '@mui/material'
import { type FC } from 'react'
import { Link } from 'react-router-dom'
import { type ICharacter } from '../../../../interfaces/characterList'
import { getIdFromUrl } from '../../../../utils/getIdFromUrl'
import GenderIcon from './GenderIcon'

interface ICharacterCardProps {
  character: ICharacter
}

const CharacterCard: FC<ICharacterCardProps> = ({ character }) => {
  return (
    <Card>
      <CardHeader
        title={
          <Typography variant="h5" component="div">
            {character.name}
          </Typography>
        }
        subheader={
          <Typography variant='body2' >
            {character.gender}
          </Typography>
          }
      />
      <CardContent>
        <GenderIcon gender={character.gender} />
      </CardContent>
      <CardActions>
        <Box display="flex" justifyContent="center" width="100%" marginBottom={'1rem'}>
          <Link to={`/character/${getIdFromUrl(character.url)}`}>
            <Button variant="outlined" color="warning" size="small">
              Learn More
            </Button>
          </Link>
        </Box>
      </CardActions>
    </Card>
  )
}

export default CharacterCard
