import { Box, Grid, Pagination } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useGetCharactersQuery } from '../../../api/swapi'
import { type ICharacter } from '../../../interfaces/characterList'
import { type RootState } from '../../../store/reducers'
import Loader from '../../common/Loader'
import CharacterCard from './CharacterCard'
import SearchInput from './SearchInput'

const CharacterList: React.FC = () => {
  const [page, setPage] = useState<number>(1)
  const { search } = useSelector((state: RootState) => state.common) // todo

  const {
    data: charactersList,
    isLoading,
    isError
  } = useGetCharactersQuery({ page, search })

  useEffect(() => {
    setPage(1)
  }, [search])

  if (isLoading) {
    return <Loader/>
  }

  if (isError || !charactersList) {
    return <div>Error occurred while fetching characters.</div>
  }

  return (
    <div>
      <SearchInput/>
      <Grid
        container
        rowSpacing={2}
        columns={{ xs: 4, sm: 8, md: 12 }}
        columnSpacing={{ xs: 1, sm: 2, md: 3 }}
      >
        {charactersList.results.map((character: ICharacter) => (
          <Grid item xs={4} key={character.url}>
            <CharacterCard character={character} />
          </Grid>
        ))}
      </Grid>
      <Box sx={{ mt: 2, mb: 2, display: 'flex', justifyContent: 'center' }}>
        {charactersList.count > 0 && (
          <Pagination
            variant="outlined"
            size='large'
            onChange={(e, page) => {
              setPage(page)
            }}
            count={Math.ceil(charactersList.count / 10)}
          />
        )}
      </Box>
    </div>
  )
}

export default CharacterList
