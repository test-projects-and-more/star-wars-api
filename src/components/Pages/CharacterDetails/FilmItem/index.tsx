import { TableCell, TableRow } from '@mui/material'
import { type FC } from 'react'
import { useGetFilmQuery } from '../../../../api/swapi'
import Loader from '../../../common/Loader'

interface IFilmItemProp {
  filmId: string
}

const FilmItem: FC<IFilmItemProp> = ({ filmId }) => {
  const { data: film, isLoading } = useGetFilmQuery(filmId)

  if (isLoading) {
    return (
      <TableRow>
        <TableCell>
          <Loader />
        </TableCell>
      </TableRow>
    )
  }

  if (!film) {
    return <TableRow>Error</TableRow>
  }

  const { title, producer } = film

  return (
    <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
      <TableCell>{title}</TableCell>
      <TableCell>{producer}</TableCell>
    </TableRow>
  )
}

export default FilmItem
