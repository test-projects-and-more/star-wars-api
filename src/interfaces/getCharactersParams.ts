export interface IGetCharactersParams {
  page?: number
  limit?: number
  search?: string
}
