# SWAPI Test Project

This project was created as a test assignment and utilizes SWAPI (Star Wars API) to fetch information about characters and planets from the Star Wars universe. It is developed using the following technologies: Yarn, React, RTK-Query, Redux Toolkit, and Material-UI (Mui).

## Installation

1. Clone the repository to your local machine:

### `git clone https://gitlab.com/test-projects-and-more/star-wars-api`
Navigate to the project directory:

### `cd swapi-test-project`
Install the dependencies using Yarn:
### `yarn install`
Running the Project
Run the following command to start the project in development mode:

### `yarn start`
Open your browser and visit http://localhost:3000 to see the application in action during development.

## Features
In this project, you will find the following features:

Character Display: Fetching and displaying a list of Star Wars characters along with their basic details.
Character Search: Ability to search for characters based on their names.
Pagination: Splitting characters and planets into pages for easier navigation.

Contributing
If you find a bug, have suggestions for improvement, or would like to contribute, please create a new issue or pull request in the project repository.


### `yarn test`

Launches the test runner in the interactive watch mode.\
