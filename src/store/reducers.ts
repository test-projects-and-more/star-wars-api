import { combineReducers } from '@reduxjs/toolkit'
import { swapiApi } from '../api/swapi'
import commonSlice from './slices/commonSlice'
import editCharSlice from './slices/editCharSlice'

const rootReducer = combineReducers({
  [swapiApi.reducerPath]: swapiApi.reducer,
  common: commonSlice,
  editChar: editCharSlice
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
