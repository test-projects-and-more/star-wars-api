import { FormControl, TextField } from '@mui/material'
import { useDispatch } from 'react-redux'
import { setSearch } from '../../../../store/slices/commonSlice'

const SearchInput = () => {
  const dispatch = useDispatch()

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setSearch(event.target.value))
  }

  return (
    <FormControl fullWidth sx={{ mb: 2 }} >
        <TextField
          id="search"
          label="Search"
          color={'primary'}
          variant='outlined'
          onChange={handleSearchChange}/>
      </FormControl>
  )
}

export default SearchInput
