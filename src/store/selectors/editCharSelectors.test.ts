import { getIsEditSelector } from './editCharSelectors'

test('getIsEditSelector should return the value of isEdit from state', () => {
  const state: any = {
    common: {
      darkMode: true
    },
    editChar: {
      isEdit: false
    }
  }

  const result = getIsEditSelector(state)

  expect(result).toBe(false)
})
