import { fireEvent, render, screen } from '@testing-library/react'
import EditTextField, { type IEditTextFieldProps } from '.'

describe('EditTextField', () => {
  const onChangeMock = jest.fn()
  const defaultProps: IEditTextFieldProps = {
    isEdit: false,
    name: 'text',
    label: 'label',
    value: 'Initial value',
    onChange: onChangeMock
  }

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('renders non-editable text when isEdit is false', () => {
    render(<EditTextField {...defaultProps} />)

    const nonEditableText = screen.getByText(defaultProps.value)
    expect(nonEditableText).toBeInTheDocument()
  })

  it('renders editable text field when isEdit is true', () => {
    render(<EditTextField {...defaultProps} isEdit={true} />)

    const textField = screen.getByRole('textbox')
    expect(textField).toBeInTheDocument()
  })

  it('calls onChange handler when text field value is changed', () => {
    render(<EditTextField {...defaultProps} isEdit={true} />)

    const textField = screen.getByRole('textbox')
    fireEvent.change(textField, { target: { value: 'New value' } })

    expect(onChangeMock).toHaveBeenCalledTimes(1)
    expect(onChangeMock).toHaveBeenCalledWith(expect.any(Object))
  })
})
