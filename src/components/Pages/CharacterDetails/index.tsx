import {
  Box,
  Button,
  Card,
  CardContent,
  Typography
} from '@mui/material'
import React, { useEffect } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import { useGetCharacterQuery } from '../../../api/swapi'
import { getIsEditSelector } from '../../../store/selectors/editCharSelectors'
import { toggleEdit } from '../../../store/slices/editCharSlice'
import EditSaveButton from '../../common/EditSaveButton'
import Loader from '../../common/Loader'
import AdditionalDetails from './AdditionalDetails'
import Description from './Description'

const CharacterDetails: React.FC = () => {
  const { id } = useParams<{ id: string }>()
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const isEdit = useSelector(getIsEditSelector)

  const toggleEditHandler = () => {
    dispatch(toggleEdit())
  }

  useEffect(() => {
    return () => {
      if (isEdit) {
        toggleEditHandler()
      }
    }
  }, [])

  const {
    data: character,
    isLoading,
    isError
  } = useGetCharacterQuery(id ?? '')

  if (isLoading) {
    return <Loader />
  }

  if (isError) {
    return <Typography variant='h4'>Error occurred while fetching character details.</Typography>
  }

  if (!character) {
    return <div>Character not found.</div>
  }

  return (
    <Card>
      <Box
        sx={{
          padding: '15px',
          display: 'flex',
          justifyContent: 'space-between'
        }}
      >
        <Button
          variant="contained"
          onClick={() => {
            navigate('/')
          }}
        >
          Back to list
        </Button>
        <EditSaveButton isEdit={isEdit} onClick={toggleEditHandler} />
      </Box>

      <CardContent>
        <Description character={character} />
        <AdditionalDetails character={character} />
      </CardContent>
    </Card>
  )
}

export default CharacterDetails
