import { GlobalStyles, ThemeProvider, createTheme } from '@mui/material'
import { useMemo } from 'react'
import { useSelector } from 'react-redux'
import { RouterProvider } from 'react-router-dom'
import './App.css'
import { router } from './Router'
import { getDarkModeSelector } from './store/selectors/commonSelectors'
import { getDesignTokens } from './styles/theme'

function App () {
  const darkMode = useSelector(getDarkModeSelector)
  const mode = darkMode ? 'dark' : 'light'
  const theme = useMemo(() => createTheme(getDesignTokens(mode)), [mode])

  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <GlobalStyles
          styles={{
            body: { backgroundColor: theme.palette.background.default }
          }}
        />
          <RouterProvider router={router} />
      </ThemeProvider>
    </div>
  )
}

export default App
