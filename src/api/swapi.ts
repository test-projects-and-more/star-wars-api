import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { type ICharacter, type ICharacterList } from '../interfaces/characterList'
import { type IFilm } from '../interfaces/film'
import { type IGetCharactersParams } from '../interfaces/getCharactersParams'
import { type IPlanet } from '../interfaces/planet'
import { type ISpecies } from '../interfaces/species'
import { type IStarships } from '../interfaces/starships'
import { type IVehicle } from '../interfaces/vehicle'

export const swapiApi = createApi({
  reducerPath: 'swapi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://swapi.dev/api' }),
  endpoints: (builder) => ({
    getCharacters: builder.query<ICharacterList, IGetCharactersParams>({
      query: ({ page, search }) => ({
        url: '/people',
        params: {
          page,
          search
        }
      })
    }),
    getCharacter: builder.query<ICharacter, string>({
      query: (id: string) => `people/${id}`
    }),

    getFilm: builder.query<IFilm, string>({
      query: (id) => `films/${id}`
    }),
    getSpecies: builder.query<ISpecies, string>({
      query: (id) => `species/${id}`
    }),
    getStarship: builder.query<IStarships, string>({
      query: (id) => `starships/${id}`
    }),
    getVehicle: builder.query<IVehicle, string>({
      query: (id) => `vehicles/${id}`
    }),
    getPlanet: builder.query<IPlanet, string>({
      query: (id) => `planets/${id}`
    })
  })
})

export const {
  useGetCharactersQuery,
  useGetCharacterQuery,
  useGetFilmQuery,
  useGetSpeciesQuery,
  useGetStarshipQuery,
  useGetVehicleQuery,
  useGetPlanetQuery
} = swapiApi
