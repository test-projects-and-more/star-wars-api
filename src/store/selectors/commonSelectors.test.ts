import { getDarkModeSelector, getSearchSelector } from './commonSelectors'

const state: any = {
  common: {
    search: '',
    darkMode: true
  },
  editChar: {
    isEdit: false
  }
}

describe('common selector test', () => {
  test('getDarkModeSelector should return the value of darkMode from state', () => {
    const result = getDarkModeSelector(state)

    expect(result).toBe(true)
  })

  test('getSearchSelector should return the value of darkMode from state', () => {
    const result = getSearchSelector(state)

    expect(result).toBe('')
  })
})
