import { Typography } from '@mui/material'
import { type FC } from 'react'
import { useGetPlanetQuery } from '../../../../api/swapi'
interface IPlanetItemProp {
  planetId: string
}

const PlanetItem: FC<IPlanetItemProp> = ({ planetId }) => {
  const { data: planet, isLoading: isPlanetLoading } =
    useGetPlanetQuery(planetId)

  if (isPlanetLoading) {
    return <div>Loading planet...</div>
  }

  if (!planet) {
    return <div>Error: Unable to fetch planet data.</div>
  }

  return (
    <Typography fontWeight={'bold'}>
      Homeworld: {planet.name}
    </Typography>
  )
}

export default PlanetItem
