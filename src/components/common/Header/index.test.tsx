import { fireEvent, render } from '@testing-library/react'
import { Provider } from 'react-redux'
import { MemoryRouter, Route, Routes } from 'react-router-dom'
import configureStore, { type MockStoreEnhanced } from 'redux-mock-store'
import Header from '.'
import { toggleDarkMode } from '../../../store/slices/commonSlice'

const mockStore = configureStore([])

describe('Header', () => {
  // eslint-disable-next-line @typescript-eslint/ban-types
  let store: MockStoreEnhanced<unknown, {}>

  beforeEach(() => {
    store = mockStore({})
  })

  it('dispatches toggleDarkMode action when the switch is toggled', () => {
    const { getByLabelText } = render(
      <Provider store={store}>
        <MemoryRouter>
          <Routes>
            <Route path="/" element={<Header />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    )

    const switchElement = getByLabelText('test')
    fireEvent.click(switchElement)

    const actions = store.getActions()
    expect(actions).toEqual([toggleDarkMode()])
  })
})
