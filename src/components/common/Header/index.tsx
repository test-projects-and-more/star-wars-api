import {
  AppBar,
  Container,
  Grid,
  Switch,
  Toolbar,
  Typography
} from '@mui/material'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router'
import { toggleDarkMode } from '../../../store/slices/commonSlice'
import SWLogo from '../../icons/SWLogo'

function Header () {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const toggleDarkModeHandler = () => {
    dispatch(toggleDarkMode())
  }

  const handleLogoClick = () => {
    navigate('/')
  }

  return (
    <AppBar color="primary" position="static" enableColorOnDark sx={{ mb: 2 }}>
      <Container maxWidth="lg">
        <Toolbar sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <SWLogo width={'6rem'} onClick={handleLogoClick} />
          <div>
            <Grid component="label" container alignItems="center" spacing={1}>
              <Grid item>
                <Typography >Light</Typography>
              </Grid>

              <Grid item>
                <Switch
                  color="secondary"
                  aria-label="test"
                  onChange={toggleDarkModeHandler}
                />
              </Grid>
              <Grid item>
                <Typography color={'black'}>Dark</Typography>
              </Grid>
            </Grid>
          </div>
        </Toolbar>
      </Container>
    </AppBar>
  )
}

export default Header
