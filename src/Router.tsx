import { createBrowserRouter } from 'react-router-dom'
import CharacterDetails from './components/Pages/CharacterDetails'
import CharacterList from './components/Pages/CharacterList'
import Layout from './components/common/Layout'

export const router = createBrowserRouter([
  {
    element: <Layout />,
    children: [
      {
        path: '/',
        element: <CharacterList />
      },
      {
        path: '/character/:id',
        element: <CharacterDetails />
      }
    ]
  }
])
