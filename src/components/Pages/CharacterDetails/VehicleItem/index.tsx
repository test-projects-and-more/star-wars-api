import { TableCell, TableRow } from '@mui/material'
import { type FC } from 'react'
import { useGetVehicleQuery } from '../../../../api/swapi'
import Loader from '../../../common/Loader'

interface IVehicleItemProp {
  vehicleId: string
}

const VehicleItem: FC<IVehicleItemProp> = ({ vehicleId }) => {
  const { data: vehicle, isLoading } = useGetVehicleQuery(vehicleId)

  if (isLoading) {
    return (
      <TableRow>
        <TableCell>
          <Loader />
        </TableCell>
      </TableRow>
    )
  }

  if (!vehicle) {
    return <TableRow>Error</TableRow>
  }

  const { name, model, vehicle_class, cost_in_credits } = vehicle

  return (
    <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
      <TableCell>{name}</TableCell>
      <TableCell>{model}</TableCell>
      <TableCell>{vehicle_class}</TableCell>
      <TableCell>{cost_in_credits}</TableCell>
    </TableRow>
  )
}

export default VehicleItem
