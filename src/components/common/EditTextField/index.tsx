import { Box, TextField, Typography } from '@mui/material'
import { type ChangeEventHandler, type FC } from 'react'

export interface IEditTextFieldProps {
  isEdit: boolean
  name: string
  label?: string
  value: string
  onChange: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>
}

const EditTextField: FC<IEditTextFieldProps> = (props) => {
  const { isEdit, name, value, onChange, label } = props

  return (
    <Box display={'flex'} sx={{ m: 1 }} alignItems={'center'}>
      <Typography sx={{ mr: 1 }} fontWeight={'bold'}>
        {label ?? name}
      </Typography>
      {isEdit
        ? <TextField variant='standard' name={name} value={value} onChange={onChange} size='small' />
        : <Typography>{value}</Typography>
      }
    </Box>
  )
}

export default EditTextField
