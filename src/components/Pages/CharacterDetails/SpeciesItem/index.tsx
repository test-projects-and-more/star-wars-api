import { TableCell, TableRow } from '@mui/material'
import { type FC } from 'react'
import { useGetSpeciesQuery } from '../../../../api/swapi'
import Loader from '../../../common/Loader'

interface ISpeciesItemProp {
  speciesId: string
}

const SpeciesItem: FC<ISpeciesItemProp> = ({ speciesId }) => {
  const { data: species, isLoading } = useGetSpeciesQuery(speciesId)

  if (isLoading) {
    return (
      <TableRow>
        <TableCell>
          <Loader />
        </TableCell>
      </TableRow>
    )
  }

  if (!species) {
    return <TableRow>Error</TableRow>
  }

  const { name, classification, designation } = species
  return (
    <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
      <TableCell>{name}</TableCell>
      <TableCell>{classification}</TableCell>
      <TableCell>{designation}</TableCell>
    </TableRow>
  )
}

export default SpeciesItem
