import { type FC } from 'react'
import FGender from '../../../../icons/FGender'
import MGender from '../../../../icons/MGender'
import NoneGender from '../../../../icons/NoneGender'

interface IGenderIconProps {
  width?: string
  gender?: string
}

const GenderIcon: FC<IGenderIconProps> = ({ gender, width = '200px' }) => {
  switch (gender) {
    case 'male':
      return <MGender width={width}/>

    case 'female':
      return <FGender width={width}/>

    default:
      return <NoneGender width={width}/>
  }
}

export default GenderIcon
