import { render } from '@testing-library/react'
import { Provider } from 'react-redux'
import { MemoryRouter } from 'react-router-dom'
import { store } from '../../store'

export const renderWithRouterRedux = (
  component: React.ReactNode,
  url: string
) => {
  return render(
    <Provider store={store}>
      <MemoryRouter initialEntries={[url]}>
        {component}
        </MemoryRouter>
    </Provider>
  )
}
