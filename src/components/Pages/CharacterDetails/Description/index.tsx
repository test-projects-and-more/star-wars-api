import { Box, Grid, Typography } from '@mui/material'
import { useState, type FC } from 'react'
import { useSelector } from 'react-redux'
import { type ICharacter } from '../../../../interfaces/characterList'
import { type RootState } from '../../../../store/reducers'
import { getIdFromUrl } from '../../../../utils/getIdFromUrl'
import EditTextField from '../../../common/EditTextField'
import GenderIcon from '../../CharacterList/CharacterCard/GenderIcon'
import PlanetItem from '../HomeworldItem'

interface IDescriptionProps {
  character: ICharacter
}

const Description: FC<IDescriptionProps> = ({ character }) => {
  const isEdit = useSelector((state: RootState) => state.editChar.isEdit)
  const [localChar, setlocalChar] = useState<ICharacter>(character)

  const {
    name,
    birth_year,
    gender,
    eye_color,
    hair_color,
    height,
    mass,
    skin_color
  } = localChar

  const onChangeFieldhandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target

    setlocalChar((prevFormData) => ({
      ...prevFormData,
      [name]: value
    }))
  }

  return (
    <Box sx={{ flexGrow: 1, mb: 2 }}>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} md={6}>
          <Typography variant="h5" fontWeight={'bold'}>
            {name}
          </Typography>
          <GenderIcon gender={gender} />
          <PlanetItem planetId={getIdFromUrl(character.homeworld)} />
        </Grid>
        <Grid item xs={12} sm={6} md={6} textAlign={'left'}>
          <Typography variant="h5" fontWeight={'bold'}>
            Description:
          </Typography>
          <EditTextField
            name="birth_year"
            label="Birth Year:"
            isEdit={isEdit}
            onChange={onChangeFieldhandler}
            value={birth_year}
          />
          <EditTextField
            name="eye_color"
            label="Eye Color:"
            isEdit={isEdit}
            onChange={onChangeFieldhandler}
            value={eye_color}
          />
          <EditTextField
            name="gender"
            label="Gender:"
            isEdit={isEdit}
            onChange={onChangeFieldhandler}
            value={gender}
          />
          <EditTextField
            name="hair_color"
            label="Hair Color:"
            isEdit={isEdit}
            onChange={onChangeFieldhandler}
            value={hair_color}
          />
          <EditTextField
            name="height"
            label="Height:"
            isEdit={isEdit}
            onChange={onChangeFieldhandler}
            value={height}
          />
          <EditTextField
            name="mass"
            label="Mass:"
            isEdit={isEdit}
            onChange={onChangeFieldhandler}
            value={mass}
          />
          <EditTextField
            name="skin_color"
            label="Skin Color:"
            isEdit={isEdit}
            onChange={onChangeFieldhandler}
            value={skin_color}
          />
        </Grid>
      </Grid>
    </Box>
  )
}

export default Description
