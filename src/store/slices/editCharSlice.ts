import { createSlice } from '@reduxjs/toolkit'

interface IEditCharState {
  isEdit: boolean
}

const initialState: IEditCharState = {
  isEdit: false
}

const editCharSlice = createSlice({
  name: 'editChar',
  initialState,
  reducers: {
    toggleEdit: (state) => {
      state.isEdit = !state.isEdit
    }
  }
})

export const { toggleEdit } = editCharSlice.actions
export default editCharSlice.reducer
