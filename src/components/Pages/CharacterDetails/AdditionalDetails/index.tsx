import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@mui/material'
import { type FC } from 'react'
import { type ICharacter } from '../../../../interfaces/characterList'
import { getIdFromUrl } from '../../../../utils/getIdFromUrl'
import FilmItem from '../FilmItem'
import SpeciesItem from '../SpeciesItem'
import StarshipItem from '../StarshipItem'
import VehicleItem from '../VehicleItem'

interface IAdditionalDetailsProps {
  character: ICharacter
}

const AdditionalDetails: FC<IAdditionalDetailsProps> = ({ character }) => {
  return (
    <>
      {character.films.length > 0 && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Films</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Title</TableCell>
                    <TableCell>Producer</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {character.films.map((film) => {
                    return <FilmItem key={film} filmId={getIdFromUrl(film)} />
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </AccordionDetails>
        </Accordion>
      )}

      {character.species.length > 0 && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Species</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Classification</TableCell>
                    <TableCell>Designation</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {character.species.map((species) => {
                    return (
                      <SpeciesItem
                        key={species}
                        speciesId={getIdFromUrl(species)}
                      />
                    )
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </AccordionDetails>
        </Accordion>
      )}

      {character.starships.length > 0 && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Starships</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Model</TableCell>
                    <TableCell>Starship class</TableCell>
                    <TableCell>Cost in credits</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {character.starships.map((starship) => {
                    return (
                      <StarshipItem
                        key={starship}
                        starshipId={getIdFromUrl(starship)}
                      />
                    )
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </AccordionDetails>
        </Accordion>
      )}

      {character.vehicles.length > 0 && (
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Vehicles</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Model</TableCell>
                    <TableCell>Vehicle class</TableCell>
                    <TableCell>Cost in credits</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {character.vehicles.map((vehicle) => {
                    return (
                      <VehicleItem
                        key={vehicle}
                        vehicleId={getIdFromUrl(vehicle)}
                      />
                    )
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </AccordionDetails>
        </Accordion>
      )}
    </>
  )
}

export default AdditionalDetails
