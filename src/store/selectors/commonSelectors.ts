import { type RootState } from '../reducers'

export const getDarkModeSelector = (state: RootState) => state.common.darkMode
export const getSearchSelector = (state: RootState) => state.common.search
