import { type PaletteMode } from '@mui/material'

export const getDesignTokens = (mode: PaletteMode) => ({
  palette: {
    mode,
    ...(mode === 'dark'
      ? {
          // palette values for dark mode
          primary: {
            main: '#860F0F'
          },
          secondary: {
            main: '#000000'
          },
          error: {
            main: '#f44336'
          },
          background: {
            paper: '#424242',
            default: '#303030'
          }
        }
      : {
          // palette values for light mode
          primary: {
            main: '#196aaf'
          },
          secondary: {
            main: '#21467a'
          },
          error: {
            main: '#f44336'
          },
          background: {
            default: '#f3eeea'
          }
        })
  }
})
